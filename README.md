# sql-camino

## Dependencies
* docker
* docker-compose

## init.db
* Contain shell and sql scripts ran on docker build.
* Obs: only work to fresh created databases.

## Run
1. Source project aliases `.aliases`
2. You can access the psql console of db direct with: `camino-psql`
3. You can find the answer to the challenge with `resposta_N` command.
* Ex: resposta_1
