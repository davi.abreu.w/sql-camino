SELECT movie_countries.country, movies.name
  FROM movies
    JOIN movie_references ON movie_references.movie_id = movies.id
    JOIN movie_countries ON movie_countries.movie_id = movies.id
      WHERE movie_countries.country IN  (
                                          SELECT movie_countries.country
                                            FROM movies   
                                              JOIN movie_countries ON movie_countries.movie_id = movies.id
                                              GROUP BY movie_countries.country
                                              ORDER BY count(movie_countries.country) DESC
                                              LIMIT 5
                                        )
      AND movie_references.type = 'Influence'
      ORDER BY movie_countries.country ASC
;

-- GET TOP 5 Country Movie Producers

-- SELECT movie_countries.country, count(*)
--   FROM movies   
--     JOIN movie_countries ON movie_countries.movie_id = movies.id
--     GROUP BY movie_countries.country
--     ORDER BY count(movie_countries.country) DESC
--     LIMIT 5