SELECT people.name
  FROM people
    JOIN casts ON casts.person_id = people.id
    JOIN jobs ON casts.job_id = jobs.id
    JOIN movies ON casts.movie_id = movies.id
    JOIN movie_countries ON movie_countries.movie_id = movies.id
    WHERE people.gender = 1
      AND movie_countries.country = 'ES'
      AND jobs.name = 'Actor'
      OR jobs.name = 'Actors'
    GROUP BY people.name
    HAVING count(people.name) > 5
;
