WITH update_element as (
  SELECT people.id, UPPER(people.name) as upper_name
    FROM movies
      JOIN casts ON casts.movie_id = movies.id
      JOIN jobs ON casts.job_id = jobs.id
      JOIN people ON casts.person_id = people.id
      WHERE movies.date > '2000-01-01'
        AND jobs.name = 'Director'
)

UPDATE people
SET name = update_element.upper_name
FROM update_element
WHERE people.id = update_element.id
;


-- Temporary

-- SELECT UPPER(people.name)
--   FROM movies
--     JOIN casts ON casts.movie_id = movies.id
--     JOIN jobs ON casts.job_id = jobs.id
--     JOIN people ON casts.person_id = people.id
--     WHERE movies.date > '2000-01-01'
--       AND jobs.name = 'Director'
-- ;
