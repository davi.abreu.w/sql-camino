SELECT  CASE
          WHEN date < '01/01/1900' THEN '1. Antes de 1900 (incluso)'
          WHEN date BETWEEN '1901-01-01' AND '1920-01-01' THEN '2. Entre 1901 e 1920 (incluso)'
          WHEN date BETWEEN '1921-01-01' AND '1940-01-01' THEN  '3. Entre 1921 e 1940 (incluso)'
          WHEN date BETWEEN '1941-01-01' AND '1960-01-01' THEN '4. Entre 1941 e 1960 (incluso)'
          WHEN date BETWEEN '1961-01-01' AND '1980-01-01' THEN '5. Entre 1961 e 1980 (incluso)'
          WHEN date BETWEEN '1981-01-01' AND '2000-01-01' THEN '6. Entre 1981 e 2000 (incluso)'
          WHEN date > '2000-01-01' then '7. Depois de 2000'
        END AS period
, count(movies.id)
  FROM movies
  GROUP BY period
  ORDER BY period ASC
;