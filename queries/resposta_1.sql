SELECT people.name
  FROM people
    JOIN casts ON casts.person_id = people.id
    JOIN jobs ON casts.job_id = jobs.id
    JOIN movies ON casts.movie_id = movies.id
    JOIN movie_countries ON movie_countries.movie_id = movies.id
    WHERE people.gender = 0
      AND movie_countries.country = 'BR'
      AND jobs.name = 'Actor'
      OR jobs.name = 'Actors'
    GROUP BY people.name
    ORDER BY count(people.name) DESC
    LIMIT 5
;
